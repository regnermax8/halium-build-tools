#!/bin/sh -xe
device="$1"
ver="$2"

cd "/opt/halium_build/$ver"

rm -f ./.repo/local_manifests/*
repo sync -j10 -c --force-sync --force-remove-dirty
JOBS=10 ./halium/devices/setup "$device" --force-sync --force-remove-dirty
