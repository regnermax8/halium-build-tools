#!/bin/bash -xe

workspace="$1"
androidsource="$2"

git log --pretty='%H' -1 > "$workspace/used-repos.txt"

pushd "$workspace"
    git log --pretty='%H' -1 >> "$workspace/used-repos.txt"
popd

cd "$androidsource"

{
    repo forall -p -c 'git log --pretty="%H" -1'
    repo status
    repo diff
} >> "$workspace/used-repos.txt"
if cmp --quiet "$workspace/last-used-repos.txt" "$workspace/used-repos.txt"; then
    echo 'This is the same build as last time, exiting.'
    exit 0
fi

exit 1
